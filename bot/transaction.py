import random
import json
import requests
import jwt

from marvel_api import MarvelRequest
from telebot import types
import os
from dotenv import load_dotenv

load_dotenv()


class Transaction:
    url = os.getenv('TRANSACTION_URL')
    request_payload = {
        "mode": "test",
        "processor": "stripe",
        "credentials": {
            "api_key": os.getenv('STRIPE_KEY')
        },
        "transaction_type": "sale",
        "interaction_type": "web",
        "tender_type": "credit_card",
        'amount': float(f'{random.randint(0, 29)}.99'),
    }

    def __init__(self, bot, token):
        self.item_code = None
        self.item_name = None
        self.item_id = None
        self.bot = bot
        self.jwt = token
        self.request_transaction = self.request_payload
        self.request_transaction['amount'] = float(f'{random.randint(0, 29)}.99')

    def process_data(self, message, attr, process_func, next_process_func):
        if message.text.isdecimal():
            if attr not in self.request_transaction.keys():
                self.request_transaction[attr] = message.text
            next_process_func(message)
        else:
            message = self.bot.send_message(message.chat.id, "Only numbers allowed.")
            process_func(message)

    def get_card_account_number(self, message):
        message = self.bot.send_message(message.chat.id, "Card account number:", reply_markup=types.ForceReply())
        self.bot.register_next_step_handler(message, self.process_data, 'card_account_number',
                                            self.get_card_account_number,
                                            self.get_card_expiry_month)

    def get_card_expiry_month(self, message):
        self.bot.delete_message(message.chat.id, message.id)
        message = self.bot.send_message(message.chat.id, "Card expiry month (MM):", reply_markup=types.ForceReply())
        self.bot.register_next_step_handler(message, self.process_data, 'card_expiry_month',
                                            self.get_card_expiry_month,
                                            self.get_card_expiry_year)

    def get_card_expiry_year(self, message):
        self.bot.delete_message(message.chat.id, message.id)
        message = self.bot.send_message(message.chat.id, "Card expiry year (YY):", reply_markup=types.ForceReply())
        self.bot.register_next_step_handler(message, self.process_data, 'card_expiry_year',
                                            self.get_card_expiry_year,
                                            self.get_card_verification_value)

    def get_card_verification_value(self, message):
        self.bot.delete_message(message.chat.id, message.id)
        message = self.bot.send_message(message.chat.id, "CVC:", reply_markup=types.ForceReply())
        self.bot.register_next_step_handler(message, self.process_data, 'card_verification_value',
                                            self.get_card_verification_value,
                                            self.send_request)

    def _validate(self, message):
        valid = True
        # Validate if card number has a length of 16
        if not len(self.request_transaction['card_account_number']) == 16:
            self.bot.send_message(message.chat.id, "Card account number invalid.", reply_markup=types.ForceReply())
            valid = False
        # Validate if card expiry month and year comes in correct format of length of 2
        if not (len(self.request_transaction['card_expiry_month']) == 2 or not len(self.request_transaction['card_expiry_year']) == 2):
            self.bot.send_message(message.chat.id, "Card expiry date invalid.", reply_markup=types.ForceReply())
            valid = False
        # Validate if CVC has 4 or less digits
        if len(self.request_transaction['card_verification_value']) > 4:
            self.bot.send_message(message.chat.id, "CVC invalid.", reply_markup=types.ForceReply())
            valid = False
        # If something is not valid, ask for card data again
        if not valid:
            self.get_card_account_number(message)
        return valid

    def send_request(self, message):
        self.bot.delete_message(message.chat.id, message.id)
        valid_data = self._validate(message)
        if valid_data:
            if not self.item_id:
                # Get article info, either it is a character or a comic
                if 'comic_id' in self.request_transaction:
                    self.item_id = self.request_transaction.pop('comic_id')
                    self.item_name = 'Comic'
                    self.item_code = 'CO'
                else:
                    self.item_id = self.request_transaction.pop('character_id')
                    self.item_name = 'Character'
                    self.item_code = 'CH'
            # We assign the Json Web Token to the request header
            headers = {
                'Authorization': f'Bearer {self.jwt["access"]}'
            }

            self.bot.send_message(message.chat.id, 'Purchase in progress...')
            # We send the request to buy
            transaction_response = requests.post(self.url + 'transactions/1/', json=json.dumps(self.request_transaction), headers=headers)
            if transaction_response.status_code == 200:
                # If it is successful, we retrieve the comic to return it to user
                marvel_api = MarvelRequest(self.bot, f'{self.item_name.lower()}s?id={self.item_id}&')
                marvel_response = marvel_api.marvel_request(marvel_api.query)
                if marvel_response.status_code == 200:
                    marvel_response = marvel_response.json()['data']['results'][0]
                    img_url = (marvel_response["images"][0]["path"] + "." + marvel_response["images"][0][
                        "extension"]) if \
                        "images" in marvel_response.keys() else ""
                    transaction_response = transaction_response.json()['data'][0]['processor_response']
                    purchase_data = {
                        'user': message.from_user.username,
                        'amount': transaction_response['amount'] / 100,
                        'item_name': self.item_code,
                        'item_id': marvel_response['id'],
                        'image_url': f'{img_url}',
                        'last_card_digits': transaction_response['payment_method_details']['card']['last4']
                    }
                    # We save the purchase in the database of the Transactions API
                    purchase_response = requests.post(self.url + 'purchases/', json=purchase_data, headers=headers)
                    if purchase_response.status_code == 201:
                        self.bot.send_message(message.chat.id,
                                              f"Purchase sucessfully. {self.item_name} ID: {self.item_id}\n"
                                              f"Name: {marvel_response['title']}\n"
                                              f"{img_url if img_url else 'URL not available.'}")
                    else:
                        self.bot.send_message(message.chat.id,
                                              f"Error saving purchase. Please, try again.\n{json.dumps(json.loads(purchase_response.text), indent=4, sort_keys=True)}")
                        self.get_card_account_number(message)

                else:
                    self.bot.send_message(message.chat.id,
                                          f"Error retrieving comic. Please, try again.\n{json.dumps(json.loads(marvel_response.text), indent=4, sort_keys=True)}")
                    self.get_card_account_number(message)
            else:
                self.bot.send_message(message.chat.id,
                                      f"Error buying {self.item_id}. Please, try again.\n{transaction_response.json()['metadata']['errors'][0]}")
                self.get_card_account_number(message)

