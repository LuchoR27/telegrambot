import json
import time
import os
from datetime import datetime
from hashlib import md5
import random
import urllib

from telebot import types

import config
import requests
import os
from dotenv import load_dotenv

load_dotenv()


class MarvelRequest:
    PUBLIC_KEY = os.getenv('MARVEL_PUBLIC_KEY')

    def __init__(self, bot, query):
        self.bot = bot
        self.query = query

    def gen_markup(self):
        yesNoMarkup = types.ReplyKeyboardMarkup()
        yes_btn = types.KeyboardButton('Yes')
        no_btn = types.KeyboardButton('No')
        yesNoMarkup.row(yes_btn, no_btn)
        return yesNoMarkup

    def get_filter(self, message, msg_filter, api_filter, process_func):
        if message.text.lower() == 'yes':
            self.query += f'{api_filter}='
            message = self.bot.send_message(message.chat.id, f"{msg_filter}", reply_markup=types.ForceReply())
            self.bot.register_next_step_handler(message, process_func)
        else:
            process_func(message)

    def marvel_request(self, query):
        """
        Method to send requests to Marvel API
        :param query: The filters of the request
        :return response: The API response from Marvel with the results
        """
        ts = time.time()
        HASH = md5(f'{ts}{config.MARVEL_KEY}{self.PUBLIC_KEY}'.encode())
        url = f'https://gateway.marvel.com:443/v1/public/{query}ts={ts}&apikey={self.PUBLIC_KEY}&hash={HASH.hexdigest()}'
        response = requests.get(url)
        return response

    def get_comic_image(self, comic):
        """
        Method to get an image from url and create it in local storage to send it to user
        :param comic: comic that contains the image to be created
        """
        url = f'{comic["images"][0]["path"]}.{comic["images"][0]["extension"]}'
        f = open(f'image.{comic["images"][0]["extension"]}', 'wb')
        f.write(urllib.request.urlopen(url).read())
        f.close()

    def add_query(self, query):
        if not query.text.lower() == 'no':
            self.query += (query.text + '&')

    def process_title(self, message):
        self.add_query(message)
        message = self.bot.send_message(message.chat.id, "Do you want to order them?",
                                        reply_markup=self.gen_markup())
        self.bot.register_next_step_handler(message, self.get_filter, "Order", "orderBy", self.process_comic_order)

    def process_name(self, message):
        self.add_query(message)
        message = self.bot.send_message(message.chat.id, "Do you want to order them?",
                                        reply_markup=self.gen_markup())
        self.bot.register_next_step_handler(message, self.get_filter, "Order", "orderBy",
                                            self.process_characters_order)

    def process_characters_order(self, message):
        self.add_query(message)
        self.send_characters_message(message)

    def process_comic_order(self, message):
        self.add_query(message)
        self.send_comics_message(message)

    def process_character_name(self, message):
        self.add_query(message)
        self.send_characters_message(message, fields=['name', 'description'])

    def send_characters_message(self, message, fields=None):
        if fields is None:
            fields = ['name', 'id', 'description', 'price', 'comics']
        response = json.loads(self.marvel_request(self.query).text)
        if response['code'] == 200:
            if response['data']['results']:
                for character in response['data']['results']:
                    message.text = ""
                    # Append fields value of response
                    for field in fields:
                        if field in character.keys() and character[field]:
                            message.text += f'{field.capitalize()}: {character[field]}\n'

                        # Set random price
                        elif field == 'price':
                            message.text += f'Price: ${random.randint(0, 29)}.99\n'

                        # List comics
                        elif field == 'comics':
                            if len(character['comics']['items']) > 0:
                                message.text += 'Comics:\n'
                                for comic in character['comics']['items']:
                                    message.text += comic['name'] + '\n'
                            else:
                                message.text += 'Comics not found.\n'
                        else:
                            message.text += f'{field.capitalize()} not found.\n'
                    self.bot.send_message(message.chat.id, message.text)
            else:
                self.bot.send_message(message.chat.id, f'Not comics found.\n')
        else:
            self.bot.send_message(message.chat.id, response['status'])

    def send_comics_message(self, message, fields=None):
        if fields is None:
            fields = ['title', 'date', 'price', 'image']
        response = json.loads(self.marvel_request(self.query).text)
        if response['code'] == 200:
            if response['data']['results']:
                message.text += self.query.split('?')[0].capitalize() + '\n\n'
                for comic in response['data']['results']:
                    message.text = ""

                    # Append fields value of response
                    for field in fields:
                        if field in comic.keys() and comic[field]:
                            message.text += f'{field.capitalize()}: {comic[field]}\n'
                        # Set random price
                        elif field == 'price':
                            message.text += f'Price: ${random.randint(0, 29)}.99\n'

                        # Get on sale date
                        elif field == 'date':
                            published_date = datetime.strptime(comic['dates'][0]['date'], '%Y-%m-%dT%H:%M:%S%z')
                            message.text += 'On sale date: ' + datetime.strftime(published_date, '%d-%m-%Y')

                        # Send image of comic
                        elif field == 'image':
                            if comic['images']:
                                self.get_comic_image(comic)
                                img = open(f'image.{comic["images"][0]["extension"]}', 'rb')
                                self.bot.send_photo(message.chat.id, img, message.text)
                                img.close()
                                os.remove(img.name)
                            else:
                                self.bot.send_message(message.chat.id, message.text)
                        else:
                            self.bot.send_message(message.chat.id, message.text)
            else:
                self.bot.send_message(message.chat.id, f'Not comics found.\n')
        else:
            self.bot.send_message(message.chat.id, response['status'])
