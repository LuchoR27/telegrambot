import codecs
from collections import OrderedDict

import requests
import json
from rest_framework import serializers

from core_payments.chd_utils import get_values_to_mask
from core_payments.models import (
    MODE_LIVE,
    MODE_TEST,
    RESULT_ERROR
)
from core_payments.requests_wrapper import Requests
from core_payments.utils import (
    validate_keys_in_dict,
    assign_if_not_none
)
from .base import AbstractProcessor
from ..models import (
    PROCESSOR_STRIPE,
    TENDER_TYPE_CREDIT_CARD,
    TENDER_TYPE_ACH,
    TRANSACTION_TYPE_CAPTURE,
    TRANSACTION_TYPE_VOID,
    TRANSACTION_TYPE_REFUND,
    TRANSACTION_TYPE_SALE,
    TRANSACTION_TYPE_AUTHORIZE, ACH_ACCOUNT_TYPE_INDIVIDUAL, ACH_ACCOUNT_TYPE_COMPANY
)


class StripeProcessor(AbstractProcessor):
    name = PROCESSOR_STRIPE
    version = 'v1'  # Check that
    payload = None  # TransactionRequest obj
    VERIFY_ACCOUNT_AMOUNTS = [32, 45]  # Micro transactions required for bank account verification.

    def _validate_request(self):
        # Verify that que api.key is valid in the request
        credentials = ['api_key']
        validate_keys_in_dict(credentials, self.transaction_request.credentials)

        # Validate original_transaction_id required for capture, void & refund
        if self.transaction_request.transaction_type in (TRANSACTION_TYPE_CAPTURE, TRANSACTION_TYPE_VOID,
                                                         TRANSACTION_TYPE_REFUND) \
                and not self.transaction_request.original_transaction_id:
            raise serializers.ValidationError('original_transaction_id is required for void, capture and refund '
                                              'transactions in the provided processor '
                                              '({}).'.format(self.transaction_request.processor))

        # Validate authorize, capture & void not supported on ACH
        if self.transaction_request.tender_type == TENDER_TYPE_ACH:
            if self.transaction_request.transaction_type in [TRANSACTION_TYPE_AUTHORIZE, TRANSACTION_TYPE_CAPTURE,
                                                             TRANSACTION_TYPE_VOID]:
                raise serializers.ValidationError('Transaction type not supported by ACH processor.')

        # Validate tender type CC fields required for authorize transaction type
        if self.transaction_request.tender_type == TENDER_TYPE_CREDIT_CARD and \
                self.transaction_request.transaction_type == TRANSACTION_TYPE_AUTHORIZE:
            for field in ['card_account_number', 'card_expiry_month', 'card_expiry_year', 'card_verification_value']:
                if not getattr(self.transaction_request, field):
                    raise serializers.ValidationError('{} is required for CC transactions in the provided '
                                                      'processor.'.format(field))

        # Validate tender type ACH fields required for sale transaction type
        if self.transaction_request.tender_type == TENDER_TYPE_ACH and \
                self.transaction_request.transaction_type == TRANSACTION_TYPE_SALE:
            for field in ['bill_to_country', 'ach_name_on_account', 'ach_account_type', 'ach_routing_number',
                          'ach_account_number']:
                if not getattr(self.transaction_request, field):
                    raise serializers.ValidationError('{} is required for ACH transactions in the provided '
                                                      'processor ({}).'.format(field,
                                                                               self.transaction_request.processor))

        # Validate ach account type required on ACH transactions
        if self.transaction_request.tender_type == TENDER_TYPE_ACH and \
                self.transaction_request.ach_account_type not in [ACH_ACCOUNT_TYPE_INDIVIDUAL,
                                                                  ACH_ACCOUNT_TYPE_COMPANY]:
            raise serializers.ValidationError('ach_account_type must be individual or company in the provided '
                                              'processor ({}).'.format(self.transaction_request.processor))

        # Validate invoice_number and description not supported on the processor
        for field in ['invoice_number', 'description']:
            if getattr(self.transaction_request, field):
                raise serializers.ValidationError('{} is not supported in the provided processor '
                                                  '({}).'.format(field, self.transaction_request.processor))

    def _set_credentials(self):
        """Setups credentials fields"""
        self.api_key = self.transaction_request.credentials['api_key']

    def _set_endpoint(self):
        """Setups api endpoint"""
        self.api_url = 'https://api.stripe.com/{}/'.format(self.version)

        return self.api_url

    def _authorize(self):
        """Performs an authorize transaction."""
        # payload variable is used to build the transactionRequest object
        # we will then wrap this up with whatever top level request object is necessary

        # Generate token for the customer
        self._generate_token()

        # Generate customer for the charge
        self._generate_customer()

        # Create the payload object and process the request
        self.payload = OrderedDict()
        self.payload['amount'] = self.transaction_request.amount_in_cents
        self.payload['currency'] = 'usd'

        # Parse customer id and token source from the stripe response
        content = json.loads(self.api_response.content.decode('utf8'))
        self.transaction_request.customer_id = content['id']
        self.transaction_request.default_source = content['default_source']

        # CC tender type param capture to false, to be captured after charge exec
        if self.transaction_request.tender_type == TENDER_TYPE_CREDIT_CARD:
            self.payload['capture'] = False

        self.payload['customer'] = self.transaction_request.customer_id
        self.payload['source'] = self.transaction_request.default_source

        # Shipping info
        has_info, ship_to = self._payload_shipping_info()
        if has_info:
            self.payload.update(ship_to)

        # Url associated with the create charge endpoint
        self.api_url = self._set_endpoint() + '{}'.format('charges')

        self._process_request()

    def _capture(self):
        """Performs a capture transaction."""
        # payload variable is used to build the transactionRequest object
        # we will then wrap this up with whatever top level request object is necessary

        # Url associated with the create capture endpoint
        self.api_url = self._set_endpoint() + '{}/{}/{}'.format('charges',
                                                                self.transaction_request.original_transaction_id,
                                                                'capture')

        self._process_request()

    def _sale(self):
        """Performs a sale transaction."""
        # payload variable is used to build the transactionRequest object
        # we will then wrap this up with whatever top level request object is necessary

        # Generate token for the customer
        self._generate_token()

        # Generate customer for the charge associated with the token id
        self._generate_customer()

        # Parse customer id and token source from the stripe response
        content = json.loads(self.api_response.content.decode('utf8'))
        self.transaction_request.customer_id = content['id']
        self.transaction_request.default_source = content['default_source']

        # ACH tender type must be verified with the corresponding customer
        if self.transaction_request.tender_type == TENDER_TYPE_ACH:
            self._verify_bank_account()

        # Create the payload object and process the request
        self.payload = OrderedDict()
        self.payload['amount'] = self.transaction_request.amount_in_cents
        self.payload['currency'] = 'usd'

        self.payload['customer'] = self.transaction_request.customer_id
        self.payload['source'] = self.transaction_request.default_source

        # Shipping info
        has_info, ship_to = self._payload_shipping_info()
        if has_info:
            self.payload.update(ship_to)

        # Url associated with the create charge endpoint
        self.api_url = self._set_endpoint() + '{}'.format('charges')

        self._process_request()

    def _void(self):
        """Performs a void transaction."""
        # payload variable is used to build the transactionRequest object
        # we will then wrap this up with whatever top level request object is necessary

        # Void same as refund in CC tender type (ACH not supported)
        self._refund()

    def _refund(self):
        """Performs a refund transaction."""
        # payload variable is used to build the transactionRequest object
        # we will then wrap this up with whatever top level request object is necessary

        # Create the payload object and process the request
        self.payload = OrderedDict()

        # Url associated with the create refund endpoint
        self.api_url = self._set_endpoint() + '{}'.format('refunds')

        self.payload['charge'] = self.transaction_request.original_transaction_id

        self._process_request()

    def _parse_processor_response(self):
        # Parse processor response to JSON
        try:
            response = self.api_response.json()
        except ValueError:
            response = json.loads(codecs.decode(self.api_response.content, 'utf-8-sig'))
        # We check if we have a good response on our side from the API
        if self.api_response.status_code == requests.codes.ok:
            # Assign the response parsed into the processor_response attribute
            self.transaction_response.processor_response = response
            # Now parse the API response and return the right values
            if 'succeeded' == response['status'] or 'pending' == response['status']:
                self.transaction_response.result = 'approved'
                self.transaction_response.transaction_id = response.get('id', None)
                self.transaction_request.original_transaction_id = response.get('id', None)
                # If valid transaction_id
                if self.transaction_response.transaction_id:
                    # Refund and void responses has different structures than others transaction types
                    if self.transaction_request.transaction_type in [TRANSACTION_TYPE_REFUND, TRANSACTION_TYPE_VOID]:
                        self.transaction_response.message = response['status']
                    else:
                        self.transaction_response.message = response['outcome']['seller_message']
            else:
                # API response was bad, let's pass the error message back
                self.transaction_response.result = response['failure_code']
                self.transaction_response.message = response['failure_message']
        else:
            self.transaction_response.processor_response = response
            # Assign the error code and message
            self.transaction_response.transaction_id = self.transaction.original_transaction_id
            self.transaction_response.result = self.api_response.status_code
            self.transaction_response.message = 'HTTP {} - {}'.format(self.api_response.status_code,
                                                                      self.api_response.text)

    def _payload_billing_info(self):
        """Populates the payload with billing info if available."""

        # Create the billing dict info and return it
        bill_to = OrderedDict()
        has_changed = False
        has_changed = assign_if_not_none(bill_to, 'address[city]',
                                         self.transaction_request.bill_to_city) or has_changed
        has_changed = assign_if_not_none(bill_to, 'address[country]',
                                         self.transaction_request.bill_to_country) or has_changed
        has_changed = assign_if_not_none(bill_to, 'address[line1]',
                                         self.transaction_request.bill_to_address) or has_changed
        has_changed = assign_if_not_none(bill_to, 'address[line2]',
                                         self.transaction_request.bill_to_address) or has_changed
        has_changed = assign_if_not_none(bill_to, 'address[postal_code]',
                                         self.transaction_request.bill_to_zip) or has_changed
        has_changed = assign_if_not_none(bill_to, 'address[state]',
                                         self.transaction_request.bill_to_state) or has_changed

        return has_changed, bill_to

    def _payload_shipping_info(self):
        """Populates the payload with shipping info if available."""
        full_name = '{} {}'.format(self.transaction_request.ship_to_first_name or '',
                                   self.transaction_request.ship_to_last_name or '')

        # Create the shipping dict info and return it
        ship_to = OrderedDict()

        has_changed = False
        has_changed = assign_if_not_none(ship_to, 'shipping[address[city]]',
                                         self.transaction_request.ship_to_city) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address[country]]',
                                         self.transaction_request.ship_to_country) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address[line1]]',
                                         self.transaction_request.ship_to_address) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address[line2]]',
                                         self.transaction_request.ship_to_address) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address[postal_code]]',
                                         self.transaction_request.ship_to_zip) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address[state]]',
                                         self.transaction_request.ship_to_state) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[name]',
                                         full_name.strip()) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[phone]',
                                         self.transaction_request.ship_to_phone) or has_changed

        return has_changed, ship_to

    def _generate_token(self):
        # Create the payload object and process the request
        self.payload = OrderedDict()

        # Url associated with the create token endpoint
        self.api_url = self._set_endpoint() + '{}'.format('tokens')

        # Create the payload object with the required fields depending on the tender type and process the request
        if TENDER_TYPE_CREDIT_CARD == self.transaction_request.tender_type:
            self.payload['card[number]'] = self.transaction_request.card_account_number
            self.payload['card[exp_month]'] = self.transaction_request.card_expiry_month
            self.payload['card[exp_year]'] = self.transaction_request.card_expiry_year
            self.payload['card[cvc]'] = self.transaction_request.card_verification_value

            self.payload['card[address_line1]'] = self.transaction_request.bill_to_address
            self.payload['card[address_line2]'] = self.transaction_request.bill_to_address
            self.payload['card[address_city]'] = self.transaction_request.bill_to_city
            self.payload['card[address_state]'] = self.transaction_request.bill_to_state
            self.payload['card[address_zip]'] = self.transaction_request.bill_to_zip
            self.payload['card[address_country]'] = self.transaction_request.bill_to_country

        if TENDER_TYPE_ACH == self.transaction_request.tender_type:
            self.payload['bank_account[country]'] = self.transaction_request.bill_to_country
            self.payload['bank_account[currency]'] = 'usd'
            self.payload['bank_account[account_number]'] = self.transaction_request.ach_account_number
            self.payload['bank_account[routing_number]'] = self.transaction_request.ach_routing_number

            self.payload['bank_account[account_holder_name]'] = self.transaction_request.ach_name_on_account
            self.payload['bank_account[account_holder_type]'] = self.transaction_request.ach_account_type

        self._process_request()

    def _generate_customer(self):
        # Parse token id from the stripe response
        content = json.loads(self.api_response.content.decode('utf8'))
        token_id = content['id']
        self.transaction_request.original_token_id = token_id

        # Url associated with the create customer endpoint
        self.api_url = self._set_endpoint() + '{}'.format('customers')

        # Create the payload object and process the request
        self.payload = OrderedDict()
        self.payload['source'] = self.transaction_request.original_token_id

        # Customer billing and address info
        has_info, bill_to = self._payload_billing_info()
        if has_info:
            self.payload.update(bill_to)
        has_info, ship_to = self._payload_shipping_info()
        if has_info:
            self.payload.update(ship_to)

        self._process_request()

    def _verify_bank_account(self):
        # Url associated with the verify account endpoint
        self.api_url = self._set_endpoint() + '{}/{}/{}/{}/{}'.format('customers',
                                                                      self.transaction_request.customer_id,
                                                                      'sources',
                                                                      self.transaction_request.default_source,
                                                                      'verify')

        # Create the payload object and process the request
        self.payload = OrderedDict()
        self.payload['amounts[]'] = self.VERIFY_ACCOUNT_AMOUNTS

        self._process_request()

    def _process_request(self):
        """Makes the request to Stripe and gets the response."""
        # Set the required headers for the request
        headers = {'Authorization': 'Bearer ' + self.api_key,
                   'Content-Type': 'application/x-www-form-urlencoded'}

        # Post the request and get the response from the api
        self.api_response = requests.post(url=self.api_url, params=self.payload, headers=headers)
