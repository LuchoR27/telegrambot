import json
import os

import ffmpeg
import speech_recognition as sr
import requests
from telebot import types, TeleBot
import config
from marvel_api import MarvelRequest
from transaction import Transaction
import dateutil.parser

bot = TeleBot(config.TELEBOT_KEY)
AUTH_URL = os.getenv('AUTH_BASE_URL')
OPTIONS = ['List characters', 'View character story', 'List comics', 'Buy comic', 'Buy character', 'View purchases']
r = sr.Recognizer()


def gen_yesno_markup():
    yesNoMarkup = types.ReplyKeyboardMarkup()
    yes_btn = types.KeyboardButton('Yes')
    no_btn = types.KeyboardButton('No')
    yesNoMarkup.row(yes_btn, no_btn)
    return yesNoMarkup


def get_token(message):
    URL = AUTH_URL + 'api/token/'
    login_data = {
        'username': os.getenv('BOT_USERNAME'),
        'password': os.getenv('BOT_PASSWORD'),
    }
    response = requests.post(URL, json=json.loads(json.dumps(login_data)))
    if response.status_code == 200:
        token = response.json()
        return token
    else:
        bot.send_message(message.chat.id,
                         f"Error {response.status_code}\n{json.dumps(json.loads(response.text), indent=4, sort_keys=True)}")


def refresh_token(message, token):
    URL = AUTH_URL + 'api/token/refresh/'
    refresh_data = {
        'refresh': token['refresh']
    }
    response = requests.post(URL, json=json.loads(json.dumps(refresh_data)))
    if response.status_code == 200:
        token['access'] = response.json()['access']
        return token
    else:
        bot.send_message(message.chat.id, f"Error refreshing {response.status_code}\n{response.text}")


def verify_token(token):
    URL = AUTH_URL + 'api/token/verify/'
    verification_data = {
        'token': token['access']
    }
    response = requests.post(URL, json=json.loads(json.dumps(verification_data)))
    if response.status_code == 200:
        return True
    else:
        return False


@bot.message_handler(commands=['menu', 'start'])
def menu(message):
    menu_markup = types.ReplyKeyboardMarkup()
    itembtna = types.KeyboardButton('List characters')
    itembtnb = types.KeyboardButton('View character story')
    itembtnc = types.KeyboardButton('List comics')
    itembtnd = types.KeyboardButton('Buy comic')
    itembtne = types.KeyboardButton('Buy character')
    itembtnf = types.KeyboardButton('View purchases')
    menu_markup.row(itembtna, itembtnb, itembtnc)
    menu_markup.row(itembtnd, itembtne, itembtnf)
    bot.send_message(message.chat.id, "Please, choose an option. You can also reply with an audio.",
                     reply_markup=menu_markup)


@bot.message_handler(func=lambda message: message.text in OPTIONS)
def process_menu(message):
    token = get_token(message)
    if message.text == 'List characters':
        get_characters(message)
    if message.text == 'View character story':
        get_characters(message)
    if message.text == 'List comics':
        get_comics(message)
    if message.text == 'Buy comic':
        buy_comic(message, token)
    if message.text == 'Buy character':
        buy_character(message, token)
    if message.text == 'View purchases':
        view_purchases(message, token)


@bot.message_handler(content_types=['voice', 'audio'])
def listen(message):
    speech_text = ''
    ogg_filepath = f'{message.chat.id}.ogg'
    wav_filepath = f'{message.chat.id}.wav'
    token = get_token(message)
    # convert mp3 to wav
    file = bot.get_file(message.voice.file_id)
    downloaded_file = bot.download_file(file.file_path)
    with open(ogg_filepath, 'wb') as new_file:
        new_file.write(downloaded_file)
    ffmpeg.input(ogg_filepath).output(wav_filepath).run()
    with sr.AudioFile(wav_filepath) as source:
        # listen for the data (load audio to memory)
        audio = r.record(source)
    try:
        # recognize (convert from speech to text)
        speech_text = r.recognize_google(audio)
    except sr.RequestError as e:
        print("Could not request results; {0}".format(e))
    except sr.UnknownValueError:
        print("Unknown error occured.")
    if speech_text in OPTIONS:
        if speech_text == "List comics":
            get_comics(message)
        if speech_text == "View character story":
            get_character(message)
        if speech_text == "List characters":
            get_characters(message)
        if speech_text == "Buy comic":
            buy_comic(message, token)
        if speech_text == "Buy character":
            buy_character(message, token)
        if speech_text == "View purchases":
            view_purchases(message, token)
    else:
        bot.send_message(message.chat.id, 'Command not recognized.')
    os.remove(ogg_filepath)
    os.remove(wav_filepath)


def get_comics(message):
    marvel_api = MarvelRequest(bot, 'comics?')
    message = bot.send_message(message.chat.id, "Do you want to filter by title?\n", reply_markup=gen_yesno_markup())
    bot.register_next_step_handler(message, marvel_api.get_filter, "Title", "titleStartsWith", marvel_api.process_title)


def get_character(message):
    marvel_api = MarvelRequest(bot, 'characters?nameStartsWith=')
    message = bot.send_message(message.chat.id, "What character you want to check out?\n",
                               reply_markup=types.ForceReply())
    bot.register_next_step_handler(message, marvel_api.process_character_name)


def get_characters(message):
    marvel_api = MarvelRequest(bot, 'characters?')
    message = bot.send_message(message.chat.id, "Do you want to filter by name?\n",
                               reply_markup=gen_yesno_markup())
    bot.register_next_step_handler(message, marvel_api.get_filter, "Name", "nameStartsWith", marvel_api.process_name)


def buy_comic(message, token):
    card_transaction = None
    if verify_token(token):
        card_transaction = Transaction(bot, token)
    else:
        token = refresh_token(message, token)
        buy_comic(message, token)
    message = bot.send_message(message.chat.id, "Enter comic ID\n", reply_markup=types.ForceReply())
    bot.register_next_step_handler(message, card_transaction.process_data, 'comic_id',
                                   buy_comic,
                                   card_transaction.get_card_account_number)


def buy_character(message, token):
    card_transaction = None
    if verify_token(token):
        card_transaction = Transaction(bot, token)
    else:
        token = refresh_token(message, token)
        buy_character(message, token)
    message = bot.send_message(message.chat.id, "Enter character ID\n", reply_markup=types.ForceReply())
    bot.register_next_step_handler(message, card_transaction.process_data, 'character_id',
                                   buy_character,
                                   card_transaction.get_card_account_number)


def view_purchases(message, token):
    headers = {}
    url = os.getenv('TRANSACTION_URL') + 'purchases/'
    if verify_token(token):
        headers = {
            'Authorization': f'Bearer {token["access"]}'
        }
    else:
        token = refresh_token(message, token)
        view_purchases(message, token)

    purchases_response = requests.get(url + f'?username={message.from_user.username}', headers=headers)

    if len(json.loads(purchases_response.text)):
        result_text = "Purchases History\n\n"
        for purchase in json.loads(purchases_response.text):
            d = dateutil.parser.parse(purchase['date'])
            result_text += f"{'Comic' if purchase['item_name'] == 'CO' else 'Character'} ID: {purchase['item_id']}\n"
            result_text += f"Amount: {purchase['amount']}\n"
            result_text += f"Date: {d.strftime('%d/%m/%Y')}\n"
            result_text += f"Last card digits: {purchase['last_card_digits']}\n"
            result_text += f"{('Image: ' + purchase['image_url'] if not purchase['image_url'] == '' else 'Image not available.')}"
            bot.send_message(message.chat.id, result_text)
            result_text = ""
    else:
        result_text = "No purchases found."
        bot.send_message(message.chat.id, result_text)


@bot.message_handler(func=lambda message: message.text not in OPTIONS)
def command_not_recognized(message):
    bot.send_message(message.chat.id, 'Command not recognized')
    menu(message)


bot.infinity_polling()
