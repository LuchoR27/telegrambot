from django.conf.urls import url
from rest_framework_simplejwt.views import token_obtain_pair, token_refresh, token_verify

urlpatterns = [
    url('^api/token/$', token_obtain_pair, name='token_obtain_pair'),
    url('^api/token/refresh/$', token_refresh, name='token_refresh'),
    url('^api/token/verify/$', token_verify, name='token_verify'),
]