from django.contrib import admin


# Register your models here.
from .models import Transaction, Purchase

admin.site.register(Transaction)
admin.site.register(Purchase)

