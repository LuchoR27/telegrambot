import random

from unittest import TestCase

from . import ProcessorMixin
from ...models import PROCESSOR_STRIPE


class StripeProcessorTestCase(ProcessorMixin, TestCase):
    """Test suite for Stripe implementation."""
    processor = PROCESSOR_STRIPE
    credentials = {
        'api_key': 'sk_test_51Jgi87KWb3UOdBDC04wVDw4qHtpiW8GAAuxhe6C8njenwQayO79clKRodJ8Yw7e47FwFaHpE6hOKhooopv8hgUBJ0'
                   '0kel2ErZs'
    }

    def setUp(self):
        super().setUp()
        self.payload['credentials'] = self.credentials
        # populate the payload with some standard test data
        self.populate_payload()

        self.test_valid_card = {
            'card_test': {
                'number': '4242424242424242',
                'exp_month': '12',
                'exp_year': '21',
                'cvc': '123'
            }
        }

        self.test_valid_ACH = {
            'ach_test': {
                'account_holder_name': 'John Doe',
                'account_holder_type': 'individual',
                'routing_number': '110000000',
                'account_number': '000123456789'
            }
        }

    # Credentials and API settings tests
    def test_invalid_api_key_cc(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'authorize'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_expiry_year'] = card['exp_year']
        self.payload['card_verification_value'] = card['cvc']
        # Invalid credentials
        self.payload['credentials'] = {'api_key': 'sk_test_51Jgi87KWb3UOdBDC04wVDw4qHtpiW8GAAuxhe6C8njenwQayO79clKRod'
                                                  'J8Yw7e47FwFaHpE6hOKhooopv8hgUBJ00kel2ErZs8'}

        self._execute(assert_status_code=400)

    # _validate_request tests (CC)
    def test_no_cvc_cc_field_required(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'authorize'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_expiry_year'] = card['exp_year']
        self._execute(assert_status_code=400)

    def test_no_account_number_cc_field_required(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'authorize'
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_expiry_year'] = card['exp_year']
        self.payload['card_verification_value'] = card['cvc']
        self._execute(assert_status_code=400)

    def test_no_exp_month_cc_field_required(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'authorize'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_year'] = card['exp_year']
        self.payload['card_verification_value'] = card['cvc']
        self._execute(assert_status_code=400)

    def test_no_exp_year_cc_field_required(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'authorize'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_verification_value'] = card['cvc']
        self._execute(assert_status_code=400)

    # _validate_request tests (ACH)
    def test_no_routing_number_sale_field_required(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'sale'
        self.payload['ach_name_on_account'] = ach['account_holder_name']
        self.payload['ach_account_number'] = ach['account_number']
        self.payload['ach_account_type'] = ach['account_holder_type']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=400)

    def test_no_account_number_sale_field_required(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'sale'
        self.payload['ach_name_on_account'] = ach['account_holder_name']
        self.payload['ach_routing_number'] = ach['routing_number']
        self.payload['ach_account_type'] = ach['account_holder_type']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=400)

    def test_no_holder_name_sale_field_required(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'sale'
        self.payload['ach_account_number'] = ach['account_number']
        self.payload['ach_routing_number'] = ach['routing_number']
        self.payload['ach_account_type'] = ach['account_holder_type']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=400)

    def test_no_holder_type_sale_field_required(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'sale'
        self.payload['ach_name_on_account'] = ach['account_holder_name']
        self.payload['ach_account_number'] = ach['account_number']
        self.payload['ach_routing_number'] = ach['routing_number']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=400)

    # CC tests
    def test_auth_valid_cc(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'authorize'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_expiry_year'] = card['exp_year']
        self.payload['card_verification_value'] = card['cvc']
        self._execute(assert_status_code=200)
        self.assertEqual('approved_by_network',
                         self.response.json()['data'][0]['processor_response']['outcome']['network_status'])

    def test_auth_cc_invalid_cvc(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'authorize'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_expiry_year'] = card['exp_year']
        self._execute(assert_status_code=400)

    def test_capture_valid_cc(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'authorize'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_expiry_year'] = card['exp_year']
        self.payload['card_verification_value'] = card['cvc']
        self._execute(assert_status_code=200)
        self.assertEqual('approved_by_network',
                         self.response.json()['data'][0]['processor_response']['outcome']['network_status'])

        # On success, do capture
        self.payload['transaction_type'] = 'capture'
        self.payload['original_transaction_id'] = self.response.json()['data'][0]['processor_response']['id']
        self._execute(assert_status_code=200)
        self.assertEqual('approved_by_network',
                         self.response.json()['data'][0]['processor_response']['outcome']['network_status'])

    def test_sale_valid_cc(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'sale'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_expiry_year'] = card['exp_year']
        self.payload['card_verification_value'] = card['cvc']
        self._execute(assert_status_code=200)
        self.assertEqual('approved_by_network',
                         self.response.json()['data'][0]['processor_response']['outcome']['network_status'])

    def test_refund_valid_cc(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'sale'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_expiry_year'] = card['exp_year']
        self.payload['card_verification_value'] = card['cvc']
        self._execute(assert_status_code=200)
        self.assertEqual('approved_by_network',
                         self.response.json()['data'][0]['processor_response']['outcome']['network_status'])

        #On success, do refund
        self.payload['transaction_type'] = 'refund'
        self.payload['original_transaction_id'] = self.response.json()['data'][0]['processor_response']['id']
        self._execute(assert_status_code=200)
        self.assertEqual('succeeded',
                         self.response.json()['data'][0]['processor_response']['status'])

    def test_void_valid_cc(self):
        card = self.test_valid_card['card_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'credit_card'
        self.payload['transaction_type'] = 'sale'
        self.payload['card_account_number'] = card['number']
        self.payload['card_expiry_month'] = card['exp_month']
        self.payload['card_expiry_year'] = card['exp_year']
        self.payload['card_verification_value'] = card['cvc']
        self._execute(assert_status_code=200)
        self.assertEqual('approved_by_network',
                         self.response.json()['data'][0]['processor_response']['outcome']['network_status'])

        # On success, do void
        self.payload['transaction_type'] = 'void'
        self.payload['original_transaction_id'] = self.response.json()['data'][0]['processor_response']['id']
        self._execute(assert_status_code=200)
        self.assertEqual('succeeded',
                         self.response.json()['data'][0]['processor_response']['status'])

    # ACH tests
    def test_sale_valid_ach(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'sale'
        self.payload['ach_name_on_account'] = ach['account_holder_name']
        self.payload['ach_account_number'] = ach['account_number']
        self.payload['ach_routing_number'] = ach['routing_number']
        self.payload['ach_account_type'] = ach['account_holder_type']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=200)
        self.assertEqual('approved_by_network',
                         self.response.json()['data'][0]['processor_response']['outcome']['network_status'])

    def test_sale_ach_invalid_account_number(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'sale'
        self.payload['ach_name_on_account'] = ach['account_holder_name']
        self.payload['ach_account_number'] = '00012345678900'
        self.payload['ach_routing_number'] = ach['routing_number']
        self.payload['ach_account_type'] = ach['account_holder_type']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=400)

    def test_refund_valid_ach(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'sale'
        self.payload['ach_name_on_account'] = ach['account_holder_name']
        self.payload['ach_account_number'] = ach['account_number']
        self.payload['ach_routing_number'] = ach['routing_number']
        self.payload['ach_account_type'] = ach['account_holder_type']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=200)
        self.assertEqual('approved_by_network',
                         self.response.json()['data'][0]['processor_response']['outcome']['network_status'])

        # On success, do refund
        self.payload['transaction_type'] = 'refund'
        self.payload['original_transaction_id'] = self.response.json()['data'][0]['processor_response']['id']
        self._execute(assert_status_code=200)
        self.assertEqual('pending',
                         self.response.json()['data'][0]['processor_response']['status'])

    def test_auth_not_supported_ach(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'authorize'
        self.payload['ach_name_on_account'] = ach['account_holder_name']
        self.payload['ach_account_number'] = ach['account_number']
        self.payload['ach_routing_number'] = ach['routing_number']
        self.payload['ach_account_type'] = ach['account_holder_type']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=400)

    def test_capture_not_supported_ach(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'capture'
        self.payload['ach_name_on_account'] = ach['account_holder_name']
        self.payload['ach_account_number'] = ach['account_number']
        self.payload['ach_routing_number'] = ach['routing_number']
        self.payload['ach_account_type'] = ach['account_holder_type']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=400)

    def test_void_not_supported_ach(self):
        ach = self.test_valid_ACH['ach_test']
        client_reference_code = str(random.randint(1000000000, 9999999999))
        self.payload['client_reference_code'] = client_reference_code
        self.payload['tender_type'] = 'ach'
        self.payload['transaction_type'] = 'void'
        self.payload['ach_name_on_account'] = ach['account_holder_name']
        self.payload['ach_account_number'] = ach['account_number']
        self.payload['ach_routing_number'] = ach['routing_number']
        self.payload['ach_account_type'] = ach['account_holder_type']
        self.payload['amount'] = self.amount
        self.payload['bill_to_country'] = 'US'
        self._execute(assert_status_code=400)

# End of tests.
