from django.conf.urls import url
from rest_framework import routers
from .views import TransactionViewSet, PurchasesAPI

router = routers.DefaultRouter()
router.register(r'transactions/(?P<project_id>\d+)', TransactionViewSet, base_name='transactions')

urlpatterns = router.urls + [
    url(r'^purchases/', PurchasesAPI.as_view()),
]
